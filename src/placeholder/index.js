import React, { useState } from 'react'
import RouteTools from '../lib/RouteTools'
import CID from 'cids'

function Placeholder( props ) {
  const [ path, setPath ] = useState( '' )

  function updatePath( e ) {
    const { value } = e.target
    setPath( value )
  }

  function submit( e ) {
    e.preventDefault()
    try {
      const cid = new CID( path )
      RouteTools.go( `/${ cid.toString() }`, { reload: true } )
    } catch ( e ) {
      RouteTools.go( path, { reload: true } )
    }
  }

  return (
    <div className="placeholder">
      <form onSubmit={ submit }>
        <input
          type="text"
          placeholder="Qm..."
          value={ path }
          onChange={ updatePath }
          size="1"
          autoFocus
        />
        <button>
          go
        </button>
      </form>
    </div>
  )
}

export default Placeholder
