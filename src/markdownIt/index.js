import React, { Component } from 'react'
import Placeholder from '../placeholder'
import RouteTools from '../lib/RouteTools'
import Iterator from '../lib/Iterator'
import CID from 'cids'
import * as FileType from 'file-type'
import { withIpfs } from '../ipfs'

// remark
import { Remark } from 'react-remark'
import gfm from 'remark-gfm'
import highlight from 'remark-highlight.js'
import 'highlight.js/styles/night-owl.css'

class MarkdownIt extends Component {

  state = {
    source: undefined
  }

  componentDidMount() {
    const { addresses } = RouteTools.parse()

    if ( !addresses ) return

    const filename = RouteTools.filename( addresses.app )

    try {
      const cid = new CID( filename )
      this.loadFromCID( cid )
    } catch ( e ) {
      this.loadFromMFS( addresses.app )
    }
  }

  async loadFromCID( cid ) {
    if ( cid instanceof CID === false ) return
    const path = '/ipfs/' + cid.toString()

    this.load( path )
  }

  async loadFromMFS( path ) {
    this.load( path )
  }

  async load( path ) {
    const { node: ipfs } = this.props.withIpfs

    const stats = await ipfs.files.stat( path )
    if ( stats.type !== 'file' ) return

    if ( ! await this.isTextFile( stats.cid ) ) return

    const fileReader = ipfs.cat( stats.cid )
    const source = await this.parseFile( fileReader )

    this.setState( ( prevState, props ) => ( {
      ...prevState,
      source
    } ) )
  }

  async isTextFile( cid ) {
    if ( cid instanceof CID === false ) return

    const { node: ipfs } = this.props.withIpfs

    const headerReader = ipfs.cat( cid, {
      offset: 0,
      length: 32
    } )

    const header = ( await Iterator.reduce( headerReader, val => {
      return val
    } ) ).shift()

    const type = await FileType.fromBuffer( header )

    return type ? false : true
  }

  async parseFile( reader ) {
    const sourceArray = await Iterator.reduce( reader, val => {
      return Buffer.from( val ).toString( 'utf8' )
    } )

    return sourceArray.join()
  }

  render() {
    const { source } = this.state

    return (
      <>
        { source ? (
          <div className="paper">
            <Remark
              remarkPlugins={ [ gfm, highlight ] }
            >
              { source }
            </Remark>
          </div>
        ) : <Placeholder /> }
      </>
    )
  }
}

export default withIpfs( MarkdownIt )
