import React from 'react'
import './App.sass'
import Ipfs from './ipfs'
import NoIpfs from './noIpfs'
import MarkdownIt from './markdownIt'
import { homepage, bugs } from '../package.json'

function App() {
  return (
    <Ipfs noIpfs={ NoIpfs } links={ { homepage, issues: bugs.url } } >
      <MarkdownIt />
    </Ipfs>
  )
}

export default App
